from django.urls import path

from InfityComics import settings
from home.views import HomeView, CartDetailView
from django.conf.urls.static import static


urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('register/', CartDetailView.as_view(), name='details_cart'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



