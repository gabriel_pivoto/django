
from django.views.generic import View
from django.shortcuts import render

class HomeView(View):
    def get(self, request):
        return render(
            request,
            template_name="home.html",
            context={
                "extraContext": {
                    "title": "Home"
                }
            }
        )

class CartDetailView(View):
    def get(self, request):
        return render(
            request,
            template_name= "details_cart.html",
            context={
                "extraContext": {
                    "title": "Cart Details"
                }
            }

        )

